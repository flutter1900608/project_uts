import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../model/product.dart';



class ItemCard extends StatelessWidget {
  final Product product;
  final void Function;
  final press;
  const ItemCard({
    Key? key,
    required this.product,
    required this.press,
    this.Function,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
       onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(kDefaultPaddin),
              //untuk demo kita gunakan fixed height dan width
              //height: 180
              //width: 160
              decoration: BoxDecoration(
                color: product.color,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Hero(
                  tag: "${product.id}",
                  child: Image.asset(product.image)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: kDefaultPaddin / 4),
            child: Text(
              //Product demo list
              product.title,
              style: TextStyle(color: kTextLightColor),
            ),
          ),
          Text(
            "\$${product.price}",
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}