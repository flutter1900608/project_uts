import 'package:flutter/material.dart';
import 'package:project_uts/screens/home/home_screen.dart';
import 'package:provider/provider.dart';

// import 'package:project_uts/provider/done_tourism_provider.dart';
// import 'provider/done_tourism_provider.dart';
import 'constants.dart';




void main() {
  runApp( MyApp());
}

// class MyApp extends StatelessWidget {
//   const MyApp({super.key});

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return ChangeNotifierProvider(
//       create: (context)=> DoneTourismProvider(),
//       child: MaterialApp(
//         title: 'Contacts',
//         theme: ThemeData(),
//         home: HomeScreen(),
//       ),
//     );
//   }
// }
  class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
    Widget build(BuildContext context) {
      return MaterialApp(
      debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
      home: HomeScreen(),
    );
  }
  }





